/************************************************
 * @brief: 	Header for main project file
 * @date:	10-07-2015
 * @author:	K.Krefta
 ***********************************************/

#ifndef __MAIN_H_
#define __MAIN_H_

//#define _USE_MATH_DEFINES
//#define __FPU_USED 1
//#define USB_ENABLED 1

#ifdef USB_ENABLED
#include "../USB/Drivers/STM32F4xx_HAL_Driver/Inc/stm32f4xx_hal.h"
#include "../USB/Inc/usb_device.h"
#include "../USB/Inc/gpio.h"
#endif  // USB_ENABLED
/************************************************
 * Includes
 ************************************************/
#include <stm32f4xx.h>
#include <cmath>
#include <cstdlib>
#include <cstdio>

/*#include <errno.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>*/

#include "../inc/elist.h"
#include "../inc/hardware_port.h"
#include "../inc/convert.h"
#include "../inc/button.h"
#include "../inc/timers.h"
#include "../inc/lcd.h"
#include "../inc/button.h"
#include "../inc/i2s.h"

/************************************************
 * Defines
 ************************************************/
#define Fs 		96000
#define Fout 	192000
#define F_CPU	168000000
#define F_AHB	168000000
#define F_APB1	84000000
#define F_APB2	42000000

/************************************************
 * Structs
 ************************************************/

/************************************************
 * Public functions
 ************************************************/
//extern long _read_r(void *reent, int fd, void *buf, size_t cnt);
/************************************************
 * Public variables
 ************************************************/
extern Hardware* hardware;
/************************************************
 * Classes
 ************************************************/
#endif// MAIN_H_
