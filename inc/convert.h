#ifndef CONVERT_H_
#define CONVERT_H_

/**
    @file convert.c
    @brief convert binary data into something and vice versa
*/

#ifdef __cplusplus
 extern "C" {
#endif

#include <ctype.h>
#include <stdbool.h>

unsigned char  uint8_to_hex(char *dst, unsigned char src);
unsigned char  uint16_to_hex(char *dst, unsigned short src);
unsigned char  uint8_to_dec(char *dst, unsigned char src);
unsigned char  uint8_to_2dec(char *dst, unsigned char src);
unsigned char  dec2_to_uint8(char *dst, char *src);
unsigned char  dec4_to_uint16(unsigned short *dst, char *src);
unsigned char  dec_to_uint8(char *dst, char *src);
unsigned char  dec_to_uint16(unsigned short *dst, char *src);
unsigned char  uint32_to_8dec(char *dst, unsigned long src);
unsigned char  uint32_to_10dec(char *dst, unsigned long src);
unsigned char  uint16_to_dec(char *dst, unsigned short src);
unsigned char  uint32_to_time(char *dst, unsigned long src);
unsigned char  uint32_to_hex(char *dst, unsigned long src);
unsigned char  hex_to_uint8(char *dst, char *src);
unsigned char  hex_to_uint16(unsigned short *dst, char *src);
unsigned char  hex_to_uint32(unsigned long *dst, char *src);
unsigned char int32_to_dec(char *dst, signed long src);

#ifdef __cplusplus
}
#endif

#endif // CONVERT_H_
