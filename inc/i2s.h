#ifndef I2S_H_
#define I2S_H_

#include "../Inc/main.h"

#ifdef __cplusplus
 extern "C" {
#endif

extern volatile	uint32_t tmp_adc_data[2];
extern volatile	int32_t adc_data[2];
extern volatile	uint32_t out_data[2];
void I2S_ADC_Init(void);
void I2S_DAC_Init(void);
void SPI3_IRQHandler(void);
void DMA1_Stream3_IRQHandler(void);
void DMA1_Stream7_IRQHandler(void);
void DAC_SetOutput(int32_t data[2]);
#ifdef __cplusplus
 }
#endif

#endif  // I2S_H_
