#ifndef __TIMERS_H
#define __TIMERS_H

#include "../inc/main.h"

#ifdef __cplusplus
 extern "C" {
#endif 
void TIM3_Init(uint16_t presc, uint16_t arr);
void TIM2_Enc_Init(void);
void InitTimer3(long Prescaler,long ARR);
void TIM4_Init(int prescaler, int arr);
void InitTimer6(long Prescaler,long ARR);
void TIM2_PWM_Init(void);
void TIM1_PWM_Init(void);
void TIM3_PWM_Init(void);

#ifdef __cplusplus
 }
#endif 
 
#endif // __TIMERS_H
