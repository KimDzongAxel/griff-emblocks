#ifndef FX_VIBRATO_H_
#define FX_VIBRATO_H_

/************************************************
 * Includes
 ************************************************/
#include "main.h"
#include "Fx.h"

/************************************************
 * Defines and enumerations
 ************************************************/
#define INTERSAMPLING_PERIOD 		0.0000104167f
#define SINE_INCREMENT_PERIOD		0.0013f
#define SINE_BASE_PERIOD			0.07f
#define SINE_MAX_PERIOD				0.2f

#define STORAGE_SPACE_SAMPLES		1000
#define STORAGE_FIRST				STORAGE_SPACE_SAMPLES+1
#define STORAGE_LAST				0

/************************************************
 * Structs
 ************************************************/

/************************************************
 * Public functions
 ************************************************/

 /************************************************
  * Public variables
  ************************************************/

 /************************************************
  * Classes
  ************************************************/

class FxVibrato : public Fx {
public:
	float* 	input_data_ptr;
	float* 	output_data_ptr;

	FxVibrato(float* in_ptr, float* out_ptr);
	~FxVibrato(void);
	void	Fx_Process(void);

	void 	SetSpeed(uint8_t knob);
	uint8_t GetSpeed(void);

	void    SetBlend(uint8_t knob);
	uint8_t GetBlend(void);

private:
	void	DMA_TransferRequest(void* source_pointer, void* targer_pointer, uint16_t count);
	uint8_t _speed_knob;
	float	_speed;

    uint8_t _blend_knob;
    float   _blend;

	float	_rad; // for sinus sinf(psi)

	__fp16*	_delay_line_base_ptr;

};

#endif  // FX_VIBRATO_H
