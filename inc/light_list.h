#ifndef ELIST_H_
#define ELIST_H_

#include "../Inc/main.h"

struct Node {
	void* data_ptr;
	Node *next;
};

class EList {
public:
    // constructor
	EList(void);

    // This prepends a new value at the beginning of the list
    void addValue(void* data_ptr);
    // returns the first element in the list and deletes the Node.
    // caution, no error-checking here!
    void* popValue();
// private member
private:
    Node *head; // this is the private member variable. It is just a pointer to the first Node
};

#endif // ELIST_H_
