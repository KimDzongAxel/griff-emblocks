#ifndef BUTTON_H_
#define BUTTON_H_

#include "../Inc/main.h"

#define BOUNCE_THRESH	100

class Button {
public:
	enum Color{
		COLOR_NONE		= 0,
		COLOR_RED		= 1,
		COLOR_ORANGE	= 2,
		COLOR_GREEN		= 3
	};

	Button(GPIO_TypeDef * GPIO_LED_G, uint8_t pin_led_g, GPIO_TypeDef * GPIO_LED_R, uint8_t pin_led_r, GPIO_TypeDef * GPIOBTN, uint8_t pin_btn);
	void Event(bool event_value);
	void TimerTick(void);
	bool GetState(void);
	void SetLatch(bool x);
	bool GetLatch(void)	{return _latch;};
	void SetColor(Color c);
private:
	bool				_latch;
	bool				_state;
	int8_t 				_bounce_cnt;
	Color 				_color;
	GPIO_TypeDef*		_GPIO_LED_R;
	GPIO_TypeDef*		_GPIO_LED_G;
	GPIO_TypeDef*		_GPIO_BTN;
	uint8_t				_r_led_pin;
	uint8_t				_g_led_pin;
	uint8_t				_btn_pin;
};

#endif  // BUTTON_H_
