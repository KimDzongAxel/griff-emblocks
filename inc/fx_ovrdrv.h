#ifndef FxOvrdrv_H_
#define FxOvrdrv_H_

#include "main.h"
#include "Fx.h"

const uint16_t knob2freq[101] = {	13, 14, 15, 16, 17, 19, 20, 22, 23, 25, 27,
									29, 31, 33, 36, 38, 41, 44, 47, 51, 55, 59,
									63, 67, 72, 78, 83, 89, 96, 103, 110, 119, 127,
									136, 146, 157, 168, 181, 194, 208, 223, 239, 257, 275,
									295, 317, 340, 364, 391, 419, 450, 482, 517, 555, 595,
									638, 685, 734, 787, 845, 906, 972, 1042, 1118, 1199, 1286, 1379,
									1479, 1586, 1701, 1825, 1957, 2099, 2251, 2414, 2590, 2777, 2979,
									3195, 3426, 3675, 3941, 4227, 4534, 4863, 5215, 5593, 5999, 6434,
									6901, 7401, 7938, 8513, 9131, 9793, 10503, 11264, 12081, 12957, 13897, 14904};

#define MAX_CUT_LVL	16777216

class FxOverdrive : public Fx {
public:
	float* 	input_data_ptr;
	float* 	output_data_ptr;

	FxOverdrive(float* in_ptr, float* out_ptr);

	void 		set_gain(uint8_t new_gain_knob);
	float	 	get_gain(void){return _gain_knob;};

	void 		set_cut(uint8_t new_cut_knob);
	uint8_t		get_cut(void){return _cut_lvl_knob;};

	void 		set_tone(uint8_t new_tone_knob);
	uint8_t		get_tone(void){return _tone_knob;};

	void 		set_blend(uint8_t new_blend_knob);
	uint8_t		get_blend(void){return _blend_knob;};

	void		Fx_Process(void);
private:
	float 		_xh;

	float		_sample;
	float		_prev_sample;

	uint8_t 	_gain_knob;
	float 		_gain;

	uint8_t 	_cut_lvl_knob;
	float		_cut_lvl;

	uint8_t 	_tone_knob;
	float		_tone_c;

	uint8_t		_blend_knob;
	float		_blend;
};

#endif  // FxOvrdrv_H_
