#ifndef LCD_H_
#define LCD_H_

#include "../Inc/main.h"

#define RCCGPIOD0 RCC_AHB1ENR_GPIOEEN
#define RCCGPIOD1 RCC_AHB1ENR_GPIOEEN
#define RCCGPIOD2 RCC_AHB1ENR_GPIOEEN
#define RCCGPIOD3 RCC_AHB1ENR_GPIOEEN
#define RCCGPIOD4 RCC_AHB1ENR_GPIOEEN
#define RCCGPIOD5 RCC_AHB1ENR_GPIOEEN
#define RCCGPIOD6 RCC_AHB1ENR_GPIOEEN
#define RCCGPIOD7 RCC_AHB1ENR_GPIOCEN

#define RCCGPIOEN RCC_AHB1ENR_GPIOCEN
#define RCCGPIORS RCC_AHB1ENR_GPIOCEN

#define GPIOD0 	GPIOE
#define GPIOD1 	GPIOE
#define GPIOD2 	GPIOE
#define GPIOD3 	GPIOE
#define GPIOD4 	GPIOE
#define GPIOD5 	GPIOE
#define GPIOD6 	GPIOE
#define GPIOD7 	GPIOC
#define GPIOEN	GPIOC
#define GPIORS	GPIOC

#define PIND0	0x0001
#define PIND1	0x0002
#define PIND2	0x0004
#define PIND3	0x0008
#define PIND4	0x0010
#define PIND5	0x0020
#define PIND6	0x0040
#define PIND7	0x2000
#define PINEN	0x4000
#define PINRS	0x8000

#define LCD_CLR             0	//DB0: clear display
#define LCD_HOME            1	//DB1: return to home position
#define LCD_ENTRY_MODE      2	//DB2: set entry mode
#define LCD_ENTRY_INC       1	//DB1: increment
#define LCD_ENTRY_SHIFT     0	//DB2: shift
#define LCD_ON_CTRL         3	//DB3: turn lcd/cursor on
#define LCD_ON_DISPLAY      2	//DB2: turn display on
#define LCD_ON_CURSOR       1	//DB1: turn cursor on
#define LCD_ON_BLINK        0	//DB0: blinking cursor
#define LCD_MOVE            4	//DB4: move cursor/display
#define LCD_MOVE_DISP       3	//DB3: move display (0-> move cursor)
#define LCD_MOVE_RIGHT      2	//DB2: move right (0-> left)
#define LCD_FUNCTION        5	//DB5: function set
#define LCD_FUNCTION_8BIT   4	//DB4: set 8BIT mode (0->4BIT mode)
#define LCD_FUNCTION_2LINES 3	//DB3: two lines (0->one line)
#define LCD_FUNCTION_10DOTS 2	//DB2: 5x10 font (0->5x7 font)
#define LCD_CGRAM           6	//DB6: set CG RAM address
#define LCD_DDRAM           7	//DB7: set DD RAM address
// reading:
#define LCD_BUSY            7	//DB7: LCD is busy
#define LCD_LINES			2	//visible lines
#define LCD_LINE_LENGTH		16	//line length (in characters)
// cursor position to DDRAM mapping
#define LCD_LINE0_DDRAMADDR		0x00
#define LCD_LINE1_DDRAMADDR		0x40
#define LCD_LINE2_DDRAMADDR		0x14
#define LCD_LINE3_DDRAMADDR		0x54

#define BIG_FONT 	1
#define SMALL_FONT 	0
#define SHOW_CURS 	2
#define CURS_BLINK 	1

enum Task{
	NONE	= 0,
	STRING 	= 1,
	CMD		= 2,
	CSTM	= 3,
	XY		= 4,
};

typedef struct {
	Task 		task;
	uint8_t 	data_length;
	uint8_t* 	data_ptr;
}__attribute__((packed)) TaskParams;

typedef struct{
	uint8_t 	char_code;
	uint8_t* 	data;
	uint8_t 	index;
}__attribute__((packed)) CstmCharParams;

typedef struct{
	uint8_t char_count;
	uint8_t index;
	char	data[48];
}__attribute__((packed)) StringParams;

typedef struct {
	uint8_t cmd;
}__attribute__((packed)) CmdParams;

enum LcdTask {
	IDLE 		= 0,
	INIT		= 1,
	SEND_CMD	= 40,
	END_CMD		= 41,
	SEND_CSTM	= 70,
	END_CSTM	= 71,
	SEND_CHAR	= 30,
	END_CHAR	= 31,
	SEND_CLR	= 50,
	END_CLR		= 51,
	SEND_HOME	= 60,
	END_HOME	= 61,
};

class LCD {

public:
	void	 	TimerTick(void);
	void		Init(void);
	bool		Status(void);
				LCD(void);
	uint8_t		WriteString(const char* str);
	uint8_t		WriteCmd(uint8_t cmd);
	uint8_t		WriteCustormChar(const char* data, uint8_t char_code);
	void		GotoXY(uint8_t x, uint8_t y);
	void		Home(void);
	void		Cls(void);
	void		Show(void);
private:
	void 		_SetOutputs(bool RS, bool E, uint8_t data);

	void		_SendCmd(uint8_t cmd);
	void		_SendDeenable(void);
	void		_SendDeRs(void);
	void		_FlushParams(void);

	void		_SendString(char* x);
	void		_KillTask(TaskParams* point);

	uint16_t 				_delay_counter;
	LcdTask 				_task;
	EList					_tasklist;

	uint8_t					_params[50];
	bool					_ready;
};

#endif
