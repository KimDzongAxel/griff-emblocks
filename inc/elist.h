#ifndef ELIST_H_
#define ELIST_H_

#include <cstdlib>

struct Node {
	void* data_ptr;
	Node *next;
};

class EList {
public:
    // constructor
	EList(void);

    // This prepends a new value at the beginning of the list
    void PushValue(void* data_ptr);
    // returns the first element in the list and deletes the Node.
    // caution, no error-checking here!
    void* PopValue(void);
    void*	PopFront(void){return PopValue();};
    void*	PopBotton(void);
    unsigned char GetCount(void){return count;};
// private member
private:
    Node *head; // this is the private member variable. It is just a pointer to the first Node
    unsigned char count;
};

#endif // ELIST_H_
