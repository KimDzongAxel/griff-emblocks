#ifndef HW_PORT_H_
#define HW_PORT_H_

/************************************************
 * Includes
 ************************************************/
#include <stm32f4xx.h>
#include "../inc/Elist.h"

/************************************************
 * Defines and enumerations
 ************************************************/

/************************************************
 * Structs
 ************************************************/
 typedef struct {
     void* data_src;
     void* data_dst;
     uint16_t count;
 } DMA_REQUEST_S;// __attribute__((packed));

class Hardware {
public:
	Hardware(uint8_t stream_1);
	Hardware(uint8_t stream_1, uint8_t stream_2);
	Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3);
	Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4);
	Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5);
	Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5, uint8_t stream_6);
	Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5, uint8_t stream_6, uint8_t stream_7);
	Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5, uint8_t stream_6, uint8_t stream_7, uint8_t stream_8);
	~Hardware(void);
	void DMA_MemoryTransferRequest(void* source_pointer, void* targer_pointer, uint16_t count);
	void DMA_TimerTick(void);
	void DMA_MemoryTransferComplete(DMA_Stream_TypeDef* DMA2_StreamX);


private:
	void DMA_RealiseTransfer(void* source_pointer, void* targer_pointer, uint16_t count, DMA_Stream_TypeDef* StreamHandle);
	DMA_Stream_TypeDef* DMA_GetStreamHandle(uint8_t stream_no);

	EList* _queue;

	uint8_t _streams_cnt;
	DMA_Stream_TypeDef* _stream[8];
};

#endif  // HW_PORT_H_
