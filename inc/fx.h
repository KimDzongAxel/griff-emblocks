#ifndef Fx_H_
#define Fx_H_

#include "main.h"

class Fx{
public:
	virtual void 	Fx_Process(void) {};

private:
	uint8_t x;
};

#endif  // Fx_H_
