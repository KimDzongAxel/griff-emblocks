#ifndef FxIntOvrdrv_H_
#define FxIntOvrdrv_H_

#include "main.h"
#include "Fx.h"

#define MAX_CUT_LVL

class FxIntOverdrive : public Fx {
public:
	int32_t* 	input_data_ptr;
	int32_t* 	output_data_ptr;

	FxIntOverdrive(int32_t* in_ptr, int32_t* out_ptr);

	void 		set_gain(uint8_t new_gain);
	uint8_t 	get_gain(void){return _gain;};

	void 		set_cut(uint32_t new_cut);
	uint32_t	get_cut(void){return _cut_lvl;};

	void		Fx_Process();
private:
	uint8_t		_gain;
	uint32_t	_cut_lvl;
};

#endif  // FxIntOvrdrv_H_
