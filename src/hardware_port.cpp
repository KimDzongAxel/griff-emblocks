/************************************************
 * @brief: 	Hardware port class
 * @date:	04-07-2015
 * @author:	K.Krefta
 ***********************************************/

#include "../INC/hardware_port.h"

Hardware::Hardware(uint8_t stream) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 1;
	_stream[0] = DMA_GetStreamHandle(stream);
	_queue = new EList();
}

Hardware::Hardware(uint8_t stream_1, uint8_t stream_2) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 2;
	_stream[0] = DMA_GetStreamHandle(stream_1);
	_stream[1] = DMA_GetStreamHandle(stream_2);
	_queue = new EList();
}

Hardware::Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 3;
	_stream[0] = DMA_GetStreamHandle(stream_1);
	_stream[1] = DMA_GetStreamHandle(stream_2);
	_stream[2] = DMA_GetStreamHandle(stream_3);
	_queue = new EList();
}

Hardware::Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 4;
	_stream[0] = DMA_GetStreamHandle(stream_1);
	_stream[1] = DMA_GetStreamHandle(stream_2);
	_stream[2] = DMA_GetStreamHandle(stream_3);
	_stream[3] = DMA_GetStreamHandle(stream_4);
	_queue = new EList();
}

Hardware::Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 5;
	_stream[0] = DMA_GetStreamHandle(stream_1);
	_stream[1] = DMA_GetStreamHandle(stream_2);
	_stream[2] = DMA_GetStreamHandle(stream_3);
	_stream[3] = DMA_GetStreamHandle(stream_4);
	_stream[4] = DMA_GetStreamHandle(stream_5);
	_queue = new EList();
}

Hardware::Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5, uint8_t stream_6) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 6;
	_stream[0] = DMA_GetStreamHandle(stream_1);
	_stream[1] = DMA_GetStreamHandle(stream_2);
	_stream[2] = DMA_GetStreamHandle(stream_3);
	_stream[3] = DMA_GetStreamHandle(stream_4);
	_stream[4] = DMA_GetStreamHandle(stream_5);
	_stream[5] = DMA_GetStreamHandle(stream_6);
	_queue = new EList();
}

Hardware::Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5, uint8_t stream_6, uint8_t stream_7) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 7;
	_stream[0] = DMA_GetStreamHandle(stream_1);
	_stream[1] = DMA_GetStreamHandle(stream_2);
	_stream[2] = DMA_GetStreamHandle(stream_3);
	_stream[3] = DMA_GetStreamHandle(stream_4);
	_stream[4] = DMA_GetStreamHandle(stream_5);
	_stream[5] = DMA_GetStreamHandle(stream_6);
	_stream[6] = DMA_GetStreamHandle(stream_7);
	_queue = new EList();
}

Hardware::Hardware(uint8_t stream_1, uint8_t stream_2, uint8_t stream_3, uint8_t stream_4, uint8_t stream_5, uint8_t stream_6, uint8_t stream_7, uint8_t stream_8) {
	RCC->AHB1ENR |= RCC_AHB1ENR_DMA2EN;
	_streams_cnt = 8;
	_stream[0] = DMA_GetStreamHandle(stream_1);
	_stream[1] = DMA_GetStreamHandle(stream_2);
	_stream[2] = DMA_GetStreamHandle(stream_3);
	_stream[3] = DMA_GetStreamHandle(stream_4);
	_stream[4] = DMA_GetStreamHandle(stream_5);
	_stream[5] = DMA_GetStreamHandle(stream_6);
	_stream[6] = DMA_GetStreamHandle(stream_7);
	_stream[7] = DMA_GetStreamHandle(stream_8);
	_queue = new EList();
}


Hardware::~Hardware(void) {
    while(_queue->GetCount() != 0) {
            free(_queue->PopBotton());
    }
    delete _queue;
}

DMA_Stream_TypeDef* Hardware::DMA_GetStreamHandle(uint8_t stream_no) {
	DMA_Stream_TypeDef* handle;
	switch (stream_no) {
	case 0:
		handle = DMA2_Stream0;
		break;
	case 1:
		handle = DMA2_Stream1;
		break;
	case 2:
		handle = DMA2_Stream2;
		break;
	case 3:
		handle = DMA2_Stream3;
		break;
	case 4:
		handle = DMA2_Stream4;
		break;
	case 5:
		handle = DMA2_Stream5;
		break;
	case 6:
		handle = DMA2_Stream6;
		break;
	case 7:
		handle = DMA2_Stream7;
		break;
	default:
		handle = NULL;
		break;
	}
	return handle;
}

void Hardware::DMA_RealiseTransfer(void* source_pointer, void* targer_pointer, uint16_t count, DMA_Stream_TypeDef* DMA2_StreamX) {
	// channel 0 = 0x00 so no bits are set, the same goes for burst increments
	DMA2_StreamX->CR	&= ~DMA_SxCR_EN;
	DMA2_StreamX->CR	|= DMA_SxCR_MBURST_0 | DMA_SxCR_PBURST_0 | DMA_SxCR_MSIZE_1 | DMA_SxCR_PSIZE_1 | DMA_SxCR_MINC | DMA_SxCR_PINC | DMA_SxCR_DIR_1 | DMA_SxCR_TCIE;
	DMA2_StreamX->M0AR 	= (uint32_t)targer_pointer;
	DMA2_StreamX->PAR 	= (uint32_t)source_pointer;
	DMA2_StreamX->NDTR 	= count;
	DMA2_StreamX->FCR	|= DMA_SxFCR_DMDIS | DMA_SxFCR_FTH_0 | DMA_SxFCR_FTH_1;
	DMA2_StreamX->CR 	|= DMA_SxCR_EN;
}

void Hardware::DMA_MemoryTransferRequest(void* source_pointer, void* targer_pointer, uint16_t count) {
    DMA_REQUEST_S *req_ptr = (DMA_REQUEST_S*)calloc(sizeof(DMA_REQUEST_S), 1);
	_queue->PushValue(reinterpret_cast<void*>(req_ptr));
}

void Hardware::DMA_MemoryTransferComplete(DMA_Stream_TypeDef* DMA2_StreamX) {
	DMA2_StreamX->CR	&= ~DMA_SxCR_EN;
}

void Hardware::DMA_TimerTick(void) {
    DMA_REQUEST_S *request;
    if (_queue->GetCount())
        for (uint8_t i = 0; i < _streams_cnt; i++)
        {
            if ((_stream[i]->NDTR == 0) && (!(_stream[i]->CR & DMA_SxCR_EN))) {
                request = reinterpret_cast<DMA_REQUEST_S*>(_queue->PopBotton());
                DMA_RealiseTransfer(request->data_src, request->data_dst, request->count, _stream[i]);
                free(reinterpret_cast<DMA_REQUEST_S*>(request));
            }
        }
}
