#include "../inc/main.h"

#define MAX_OF_INT32 0x7FFFFFFF

Hardware* hardware;


int main(void)
{
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
	GPIOB->MODER |= (1 << (6*2));

	Button left(GPIOB, 0, GPIOB, 1, GPIOB, 2);
	Button up(GPIOA, 4, GPIOA, 5, GPIOA, 6);
	Button down(GPIOE, 9, GPIOE, 8, GPIOE, 7);
	Button right(GPIOC, 5, GPIOC, 4, GPIOA, 7);
	hardware = new Hardware(0, 1, 2, 3);

#ifdef USB_ENABLED
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOHEN;
	MX_USB_DEVICE_Init();
#endif  // USB_ENABLED
	LCD Disp;
	Disp.Init();
	TIM3_Init(84, 1000);
	TIM2_Enc_Init();
	I2S_ADC_Init();
	I2S_DAC_Init();
	bool updt = false;
	uint16_t tim_val = 0;
	int16_t tmp_cnt = 0;
	while(!(Disp.Status())) {
		if (TIM3->SR & TIM_SR_CC1IF) {
    		Disp.TimerTick();
    		TIM3->SR &=~ TIM_SR_CC1IF;
    	}
	}
	Disp.WriteString("tmp_cnt = ");
	Disp.GotoXY(0,1);
	Disp.WriteString("tim_val = ");
	Disp.GotoXY(0,2);
	Disp.WriteString("out_val = ");
    while(1)
    {
    	static uint16_t presc10ms = 0;
		uint32_t tempr[2];
		tempr[0] = adc_data[0];// + MAX_OF_INT32;
		tempr[1] = adc_data[0];// + MAX_OF_INT32;
		DAC_SetOutput((int32_t*)tempr);
    	if (TIM3->SR & TIM_SR_CC1IF) {
    			if(presc10ms < 9999) {
    				presc10ms++;
    			} else {
    				presc10ms = 0;
    				tim_val = TIM2->CNT;
    				char tmp[11] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','\0'};
					uint16_to_dec(tmp, tmp_cnt);
					Disp.GotoXY(10, 0);
					Disp.WriteString(tmp + '\0');
					uint16_to_dec(tmp, tim_val/4);
					Disp.GotoXY(10, 1);
					Disp.WriteString(tmp + '\0');
    				if (left.GetState()) {
        				left.SetColor(Button::COLOR_RED);
    				} else {
    					updt = true;
    					tmp_cnt--;
        				left.SetColor(Button::COLOR_GREEN);
    				}

    				if (right.GetState()) {
    					right.SetColor(Button::COLOR_RED);
    				} else {
    					updt = true;
    					tmp_cnt++;
    					right.SetColor(Button::COLOR_GREEN);
    				}
    			}
    		Disp.TimerTick();
    		left.TimerTick();
    		right.TimerTick();
    		up.TimerTick();
    		down.TimerTick();
    		TIM3->SR &=~ TIM_SR_CC1IF;
    	}
    }
}


