#include "../Inc/interrupts.h"

void TIM3_IRQHandler(void) {
	static uint8_t pres_100ms = 0;
	static uint8_t pres_1s = 0;

	if (TIM3->SR & TIM_SR_UIF) {
	// 1ms timer
			if (pres_100ms == 99) {
				// 100ms timer
				if (pres_1s == 10) {
					// 1s timer
					GPIOB->ODR ^= (1 << 6);

					pres_1s = 0;
				} else {
					pres_1s++;
				}

				pres_100ms = 0;
			} else {
				pres_100ms++;
			}
		TIM3->SR &=~ TIM_SR_UIF;
	}
}
