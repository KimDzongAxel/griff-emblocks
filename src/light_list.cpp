#include "../Inc/eList.h"


EList::EList(void) {
	head = NULL;
}

void EList::addValue(void* data_ptr){
	Node *n = reinterpret_cast<Node*>(calloc(sizeof(Node), 1));   // create new Node
	if (n == NULL)
		return;
	n->data_ptr = data_ptr;             // set value
	n->next = head;         // make the node point to the next node.
							//  If the list is empty, this is NULL, so the end of the list --> OK
	head = n;               // last but not least, make the head point at the new node.
}

void* EList::popValue(){
	Node *n = head;
	void* ret_ptr = n->data_ptr;

	head = head->next;
	free(n);
	return ret_ptr;
}
