#include "../Inc/lcd.h"

LCD::LCD(void){
	Init();
}

void LCD::Init(void) {
	_ready = false;
	RCC->AHB1ENR |= RCCGPIOD0 | RCCGPIOD1 | RCCGPIOD2 | RCCGPIOD3 | RCCGPIOD4 | RCCGPIOD5 | RCCGPIOD6 | RCCGPIOD7 | RCCGPIOEN | RCCGPIORS;
	GPIOD0->MODER |= PIND0 * PIND0;
	GPIOD1->MODER |= PIND1 * PIND1;
	GPIOD2->MODER |= PIND2 * PIND2;
	GPIOD3->MODER |= PIND3 * PIND3;
	GPIOD4->MODER |= PIND4 * PIND4;
	GPIOD5->MODER |= PIND5 * PIND5;
	GPIOD6->MODER |= PIND6 * PIND6;
	GPIOD7->MODER |= PIND7 * PIND7;

	GPIOEN->MODER |= PINEN * PINEN;
	GPIORS->MODER |= PINRS * PINRS;
	_task = INIT;
	_delay_counter = 50;
	_tasklist = EList();
}

LcdTask operator++(LcdTask &id, int) {
	LcdTask tmp = id;
	return static_cast<LcdTask>((1 + static_cast<uint8_t>(tmp)));
}
LcdTask operator +(LcdTask &id, int x) {
	LcdTask tmp = id;
	return static_cast<LcdTask>((x + static_cast<uint8_t>(tmp)));
}

void	LCD::_KillTask(TaskParams* point) {
	free(point->data_ptr);
	free(point);
}

uint8_t LCD::WriteString(const char* str) {
	TaskParams *temp = reinterpret_cast<TaskParams*>(calloc(sizeof(TaskParams), 1));
	uint8_t i = 0;
	for (; i < 40; i++) {
		if (str[i] == '\0') {
			temp->data_length = i;
			break;
		}
	}
	if (i == 40) {
		free(temp);
		return 1;  // err code for too long string or no termination mark
	}
	temp->data_ptr = reinterpret_cast<uint8_t*>(calloc(i, 1));
	for (uint8_t i = 0; i < temp->data_length; i++) {
		temp->data_ptr[i] = str[i];
	}
	temp->task = STRING;
	_tasklist.PushValue(reinterpret_cast<void*>(temp));
	return 0;
}
uint8_t LCD::WriteCmd(uint8_t cmd) {
	TaskParams *temp = reinterpret_cast<TaskParams*>(calloc(sizeof(TaskParams), 1));
	uint8_t i = 0;
	temp->data_length = 1;
	temp->data_ptr = reinterpret_cast<uint8_t*>(calloc(1,1));
	temp->data_ptr[0] = cmd;
	temp->task = CMD;
	_tasklist.PushValue(reinterpret_cast<void*>(temp));
}

uint8_t LCD::WriteCustormChar(const char *data, uint8_t char_code) {
	TaskParams *temp = reinterpret_cast<TaskParams*>(calloc(sizeof(TaskParams), 1));
	temp->data_length = 8;
	temp->data_ptr = reinterpret_cast<uint8_t*>(calloc(9, 1));
	temp->data_ptr[0] = char_code;
	for (uint8_t i = 1; i < 8+1; i++) {
		temp->data_ptr[i] = data[i];
	}
	temp->task = CSTM;
	_tasklist.PushValue(reinterpret_cast<void*>(temp));
	return 0;
}
bool LCD::Status(void){
	return _ready;
}
void LCD::_SendDeenable(void) {
	GPIOEN->ODR &= ~PINEN;
}
void LCD::_SendDeRs(void) {
	GPIORS->ODR &= ~PINRS;
}
void LCD::_FlushParams(void) {
	for (uint8_t i = 0; i < 50; i++) {
		_params[i] = 0;
	}
}
void LCD::_SendString(char* x) {
	StringParams* params = reinterpret_cast<StringParams*>(_params);
	for (uint8_t i = 0; i < 48; i++) {
		if (x[i] == '\0')
			params->char_count = i;
		params->data[i] = x[i];
	}
	params->index = 0;
	_task = SEND_CHAR;
	_delay_counter = 1;
}
void LCD::Cls(void) {
	WriteCmd(1);
}

void LCD::Show(void) {
	WriteCmd(0x0C);
}
void LCD::Home(void) {
	WriteCmd(0x02);
}
void LCD::GotoXY(uint8_t x, uint8_t y) {
	CmdParams* params = reinterpret_cast<CmdParams*>(_params);
	uint8_t DDRAMAddr = 0;
	// remap lines into proper order
	switch(y)
	{
	case 0:
		DDRAMAddr = LCD_LINE0_DDRAMADDR+x;
		break;
	case 1:
		DDRAMAddr = LCD_LINE1_DDRAMADDR+x;
		break;
	case 2:
		DDRAMAddr = LCD_LINE2_DDRAMADDR+x;
		break;
	case 3:
		DDRAMAddr = LCD_LINE3_DDRAMADDR+x;
		break;
	default:
		DDRAMAddr = LCD_LINE0_DDRAMADDR+x;
	}
	WriteCmd(1<<LCD_DDRAM | DDRAMAddr);
}
void LCD::_SetOutputs(bool rs, bool en, uint8_t data) {
	if (data & 0x80) {
		GPIOD7->ODR |= PIND7;
	} else {
		GPIOD7->ODR &= ~PIND7;
	}

	if (data & 0x40) {
		GPIOD6->ODR |= PIND6;
	} else {
		GPIOD6->ODR &= ~PIND6;
	}

	if (data & 0x20) {
		GPIOD5->ODR |= PIND5;
	} else {
		GPIOD5->ODR &= ~PIND5;
	}

	if (data & 0x10) {
		GPIOD4->ODR |= PIND4;
	} else {
		GPIOD4->ODR &= ~PIND4;
	}

	if (data & 0x08) {
		GPIOD3->ODR |= PIND3;
	} else {
		GPIOD3->ODR &= ~PIND3;
	}

	if (data & 0x04) {
		GPIOD2->ODR |= PIND2;
	} else {
		GPIOD2->ODR &= ~PIND2;
	}

	if (data & 0x02) {
		GPIOD1->ODR |= PIND1;
	} else {
		GPIOD1->ODR &= ~PIND1;
	}

	if (data & 0x01) {
		GPIOD0->ODR |= PIND0;
	} else {
		GPIOD0->ODR &= ~PIND0;
	}

	if (en) {
		GPIOEN->ODR |= PINEN;
	} else {
		GPIOEN->ODR &= ~PINEN;
	}

	if (rs) {
		GPIORS->ODR |= PINRS;
	} else {
		GPIORS->ODR &= ~PINRS;
	}
}

void LCD::TimerTick(void) {
	// 100us timer
	if (_delay_counter > 0) {
		_delay_counter--;
	} else {
		switch (static_cast<uint8_t>(_task)) {
			case INIT:
				_SetOutputs(false, false, 0x00);
				_delay_counter = 300;
				_task = static_cast<LcdTask>(INIT+2);
				break;
			case static_cast<LcdTask>(INIT+2): // one
			case static_cast<LcdTask>(INIT+4): // two
			case static_cast<LcdTask>(INIT+6): // three
				_SetOutputs(false, true, 0x30);
				_delay_counter = 5;
				_task = _task + 1;
				break;
			case static_cast<LcdTask>(INIT+8):
				_SetOutputs(false, true, 0x38 | BIG_FONT);
				_delay_counter = 5;
				_task = static_cast<LcdTask>(INIT+9);
				break;
			case static_cast<LcdTask>(INIT+10):
				_SetOutputs(false, true, 0x08);
				_delay_counter = 5;
				_task = _task + 1;
				break;
			case static_cast<LcdTask>(INIT+12):
				_SetOutputs(false, true, 0x01);
				_delay_counter = 5;
				_task = _task + 1;
				break;
			case static_cast<LcdTask>(INIT+14):
				_SetOutputs(false, true, 0x06);
				_delay_counter = 5;
				_task = _task + 1;
				break;
			case static_cast<LcdTask>(INIT+16):
				_SetOutputs(false, true, 0x0C);
				_delay_counter = 5;
				_task = _task + 1;
				break;
			case static_cast<LcdTask>(INIT+3):
			case INIT+5:
			case INIT+7:
			case INIT+9:
			case INIT+11:
			case INIT+13:
			case INIT+15:
				_SendDeenable();
				_delay_counter = 50;
				_task = _task + 1;
				break;
			case static_cast<LcdTask>(INIT+17):
			case END_CMD:
			case SEND_CSTM+31:
				_SendDeenable();
				_delay_counter = 5;
				_ready = true;
				_task=IDLE;
				break;
			case SEND_CHAR: {
				StringParams *params = reinterpret_cast<StringParams*>(&_params);
				if (params->data[params->index] == '\0') {
					_task = IDLE;
				} else {
					_SetOutputs(1, 1, params->data[params->index]);
					_delay_counter = 5;
					_task = _task + 1;
				}
			}
			break;
			case END_CHAR: {
				StringParams *params = reinterpret_cast<StringParams*>(&_params);
				_SendDeenable();
				_SendDeRs();
				params->index++;
				if (params->data[params->index] == '\0') {
					_FlushParams();
					_delay_counter = 5;
					_task = IDLE;
				} else {
					_delay_counter = 5;
					_task = SEND_CHAR;
				}
			}
			break;
			case SEND_CMD: {
				CmdParams * params = reinterpret_cast<CmdParams*>(&_params);
				_SetOutputs(0, 1, params->cmd);
				_delay_counter = 5;
				_task = _task + 1;
			}
			break;
			case SEND_CSTM+0: //0
			case SEND_CSTM+4: //1
			case SEND_CSTM+8: //2
			case SEND_CSTM+12://3
			case SEND_CSTM+16://4
			case SEND_CSTM+20://5
			case SEND_CSTM+24://6
			case SEND_CSTM+28://7
			{
				CstmCharParams *params = reinterpret_cast<CstmCharParams*>(&_params);
				_SetOutputs(1, 1, 0x40 | (((params->char_code) << 3) + params->index));
				_delay_counter = 5;
				_task = _task + 1;
			}
			break;
			case SEND_CSTM+1:
			case SEND_CSTM+3:
			case SEND_CSTM+5:
			case SEND_CSTM+7:
			case SEND_CSTM+9:
			case SEND_CSTM+11:
			case SEND_CSTM+13:
			case SEND_CSTM+15:
			case SEND_CSTM+17:
			case SEND_CSTM+19:
			case SEND_CSTM+21:
			case SEND_CSTM+23:
			case SEND_CSTM+25:
			case SEND_CSTM+27:
			case SEND_CSTM+29:{
				_SendDeenable();
				_delay_counter = 5;
				_task = _task + 1;
			}
			case SEND_CSTM+2:
			case SEND_CSTM+6:
			case SEND_CSTM+10:
			case SEND_CSTM+14:
			case SEND_CSTM+18:
			case SEND_CSTM+22:
			case SEND_CSTM+26:
			case SEND_CSTM+30: {
				CstmCharParams *params = reinterpret_cast<CstmCharParams*>(&_params);
				_SetOutputs(1, 1, *(params->data + params->index));
				params->index++;
				_delay_counter = 5;
				_task = _task + 1;
			}
			default:
				if (_tasklist.GetCount() > 0) {
//					TaskParams* task = reinterpret_cast<TaskParams*>(_tasklist.PopValue());
					TaskParams* task = reinterpret_cast<TaskParams*>(_tasklist.PopBotton());
					switch(task->task) {
						case STRING: {
							_FlushParams();
							StringParams* params = reinterpret_cast<StringParams*>(_params);
							params->char_count = task->data_length;
							for (uint8_t i = 0; i < task->data_length; i++) {
								params->data[i] = task->data_ptr[i];
							}
							params->index = 0;
							_KillTask(task);
							_task = SEND_CHAR;
							_delay_counter = 1;
							break;
							}
						case CSTM: {
							_FlushParams();
							CstmCharParams* params = reinterpret_cast<CstmCharParams*>(_params);
							params->char_code = task->data_ptr[0];
							for(uint8_t i=0; i < 8; i++) {
								params->data[i] = task->data_ptr[i+1];
							}
							params->index = 0;
							_KillTask(task);
							_task = SEND_CSTM;
							_delay_counter = 1;
							break;
						}
						case CMD: {
							_FlushParams();
							CmdParams* params = reinterpret_cast<CmdParams*>(_params);
							params->cmd = task->data_ptr[0];
							_KillTask(task);
							_task = SEND_CMD;
							_delay_counter = 1;
							break;
						}
					}

				} else {
					_SetOutputs(0,0,0);
					_delay_counter = 100;
				}
				break;
		}
	}
}
