#include "../Inc/timers.h"

void TIM3_Init(uint16_t prescaler, uint16_t arr)
{
	NVIC->ISER[0] |= (1 << TIM3_IRQn);
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	TIM3->PSC = prescaler; // no prescaler, timer counts up in sync with the peripheral clock
    TIM3->DIER |= TIM_DIER_UIE; // enable update interrupt
    TIM3->ARR = arr; // count to 1 (autoreload value 1)
    TIM3->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; // autoreload on, counter enabled
    TIM3->EGR = 1; // trigger update event to reload timer registers
}

void TIM2_Enc_Init(void) {
	RCC->AHB1ENR 	|= RCC_AHB1ENR_GPIOAEN;
	GPIOA->MODER	&= ~0x0F;
	GPIOA->MODER	|= 0x02 | (0x02 << 2);
	GPIOA->AFR[0]	|= 1 | (1 << 4);
	GPIOA->PUPDR	|= 0x01 | (0x01 << 2);
	RCC->APB1ENR 	|= RCC_APB1ENR_TIM2EN;
	TIM2->SMCR 		|= TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	TIM2->CCER		|= TIM_CCER_CC1P | TIM_CCER_CC2P;
	TIM2->CR1		|= TIM_CR1_CEN;
}

void TIM4_Enc_Init(void) {
	RCC->AHB1ENR 	|= RCC_AHB1ENR_GPIOAEN;
	GPIOA->MODER	&= ~0x0F;
	GPIOA->MODER	|= 0x02 | (0x02 << 2);
	GPIOA->AFR[0]	|= 1 | (1 << 4);
	GPIOA->PUPDR	|= 0x01 | (0x01 << 2);
	RCC->APB1ENR 	|= RCC_APB1ENR_TIM2EN;
	TIM2->SMCR 		|= TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	TIM2->CCER		|= TIM_CCER_CC1P | TIM_CCER_CC2P;
	TIM2->CR1		|= TIM_CR1_CEN;
}
/*
void TIM8_CC_IRQHandler()
{
	if(TIM8->SR & TIM_SR_UIF)
	{
		//GPIOD->ODR ^= (1<<14);
		//uwrite_str("\r>STM->\tTIM::2\t\t-> AAR/Compare IRQ");
	}
	TIM8->SR = 0x00;
	
}

void TIM8_TRG_COM_TIM14_IRQHandler()
{
	if(TIM8->SR & TIM_SR_UIF)
	{
		//GPIOD->ODR ^= (1<<14);
		//uwrite_str("\r>STM->\tTIM::2\t\t-> AAR/Compare IRQ");
	}
	TIM8->SR = 0x00;
	
}

void TIM8_IRQHandler()
{
	TIM8->SR = 0x00;
	
}

void InitTimer3(int prescaler, int arr)
{
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	  TIM3->PSC = prescaler; // no prescaler, timer counts up in sync with the peripheral clock
    TIM3->DIER |= TIM_DIER_UIE; // enable update interrupt
    TIM3->ARR = arr; // count to 1 (autoreload value 1)
    TIM3->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; // autoreload on, counter enabled
		TIM3->CR2 |= TIM_CR2_MMS_1 | TIM_CR2_CCDS;
		TIM3->DIER|= TIM_DIER_UDE;
    TIM3->EGR |= 1; // trigger update event to reload timer registers
		NVIC->ISER[0] |= (1 << TIM3_IRQn);
}

void InitTimer6(int prescaler, int arr)
{
		RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;
	  TIM6->PSC = prescaler; // no prescaler, timer counts up in sync with the peripheral clock
    TIM6->DIER |= TIM_DIER_UIE; // enable update interrupt
    TIM6->ARR = arr; // count to 1 (autoreload value 1)
    TIM6->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; // autoreload on, counter enabled
		TIM6->CR2 |= TIM_CR2_MMS_1 | TIM_CR2_CCDS;
		TIM6->DIER|= TIM_DIER_UDE;
    TIM6->EGR |= TIM_EGR_TG | 1; // trigger update event to reload timer registers
		NVIC->ISER[1] |= (1 << (TIM6_DAC_IRQn-32));
		uwrite_status("TIM::6","Init","Success",0);
}

void TIM4_Init(int prescaler, int arr) {
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	TIM4->PSC = prescaler; // no prescaler, timer counts up in sync with the peripheral clock //48Mhz
  TIM4->DIER |= TIM_DIER_UIE; // enable update interrupt
  TIM4->ARR = arr; // count to 1 (autoreload value 1)
  TIM4->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; // autoreload on, counter enabled
  TIM4->EGR |= 1; // trigger update event to reload timer registers
	NVIC->ISER[0] |= (1 << (TIM4_IRQn));
}

void TIM1_PWM_Init(void) {
		RCC->APB2ENR |= RCC_APB2ENR_TIM1EN | RCC_APB2ENR_IOPAEN;
		
		GPIOA->ODR	&=~ (0x0F << 8);
		GPIOA->CRH 	&=~ (GPIO_CRH_CNF8 | GPIO_CRH_CNF9 | GPIO_CRH_CNF10 | GPIO_CRH_CNF11 | GPIO_CRH_MODE8 | GPIO_CRH_MODE9 | GPIO_CRH_MODE10 | GPIO_CRH_MODE11);
	 	GPIOA->CRH	|=	(GPIO_CRH_MODE8_0 | GPIO_CRH_MODE9_0 | GPIO_CRH_MODE10_0 | GPIO_CRH_MODE11_0 | GPIO_CRH_CNF8_1 | GPIO_CRH_CNF9_1 | GPIO_CRH_CNF10_1 | GPIO_CRH_CNF11_1);
		
	  TIM1->PSC = 71*4; 
		TIM1->ARR = 256; // cycle will be done 2 twice every 1/1000 of a second   
    TIM1->EGR |= 1; // trigger update event to reload timer registers
	
		TIM1->BDTR |= TIM_BDTR_MOE;
		
		TIM1->CCMR1 |=  TIM_CCMR1_OC1PE | (0x06 << 4u) | TIM_CCMR1_OC2PE | (0x06 << 12u);
		TIM1->CCMR2 |=  TIM_CCMR2_OC3PE | (0x06 << 4u) | TIM_CCMR2_OC4PE | (0x06 << 12u);
		
		TIM1->CCER  |=  TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E; 
		
    TIM1->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; // autoreload on, counter enabled
		
		//NVIC->ISER[1] |= (1 << (TIM8_UP_TIM13_IRQn-32)) | (1 << (TIM8_CC_IRQn-32));
}

void TIM3_PWM_Init(void) {
		RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	
		GPIOA->ODR	&=~ (0x0F << 0);
		GPIOA->CRL 	&=~ (GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_CNF2 | GPIO_CRL_CNF3 | GPIO_CRL_MODE0 | GPIO_CRL_MODE1 | GPIO_CRL_MODE2 | GPIO_CRL_MODE3);
	 	GPIOA->CRL	|=	(GPIO_CRL_MODE0_0 | GPIO_CRL_MODE1_0 | GPIO_CRL_MODE2_0 | GPIO_CRL_MODE3_0 | GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1 | GPIO_CRL_CNF2_1 | GPIO_CRL_CNF3_1);
		
	  TIM3->PSC = 71*4;
		TIM3->ARR = 256; // cycle will be done 2 twice every 1/1000 of a second
    TIM3->EGR |= 1; // trigger update event to reload timer registers
	
		TIM3->BDTR |= TIM_BDTR_MOE;
		
		TIM3->CCMR1 |=  TIM_CCMR1_OC1PE | (0x06 << 4u) | TIM_CCMR1_OC2PE | (0x06 << 12u);
		TIM3->CCMR2 |=  TIM_CCMR2_OC3PE | (0x06 << 4u) | TIM_CCMR2_OC4PE | (0x06 << 12u);
		
		TIM3->CCER  |=  TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E;
		
    TIM3->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; // autoreload on, counter enabled
		
		NVIC->ISER[0] |= (1 << (TIM3_IRQn));
}

void TIM3_PWM_Init(void) {
		RCC->APB2ENR |= RCC_APB2ENR_IOPAEN | RCC_APB2ENR_IOPBEN;
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	
		GPIOA->ODR	&=~ (0x03 << 6);
		GPIOA->CRL 	&=~ (GPIO_CRL_CNF6 | GPIO_CRL_CNF7 | GPIO_CRL_MODE6 | GPIO_CRL_MODE7);
	 	GPIOA->CRL	|=	(GPIO_CRL_MODE6_0 | GPIO_CRL_MODE7_0 | GPIO_CRL_CNF6_1 | GPIO_CRL_CNF7_1);

		GPIOB->ODR	&=~ (0x03 << 0);
		GPIOB->CRL 	&=~ (GPIO_CRL_CNF0 | GPIO_CRL_CNF1 | GPIO_CRL_MODE0 | GPIO_CRL_MODE1);
	 	GPIOB->CRL	|=	(GPIO_CRL_MODE0_0 | GPIO_CRL_MODE1_0 | GPIO_CRL_CNF0_1 | GPIO_CRL_CNF1_1);
		
	  TIM3->PSC = 71*4; 
		TIM3->ARR = 256; // cycle will be done 2 twice every 1/1000 of a second   
    TIM3->EGR |= 1; // trigger update event to reload timer registers
	
		TIM3->BDTR |= TIM_BDTR_MOE;
		
		TIM3->CCMR1 |=  TIM_CCMR1_OC1PE | (0x06 << 4u) | TIM_CCMR1_OC2PE | (0x06 << 12u);
		TIM3->CCMR2 |=  TIM_CCMR2_OC3PE | (0x06 << 4u) | TIM_CCMR2_OC4PE | (0x06 << 12u);
		
		TIM3->CCER  |=  TIM_CCER_CC1E | TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC4E; 
		
    TIM3->CR1 |= TIM_CR1_ARPE | TIM_CR1_CEN; // autoreload on, counter enabled
		
		NVIC->ISER[0] |= (1 << (TIM3_IRQn));
}*/
