/**
    @file convert.c
    @brief convert binary data into something and vice versa
*/
#include "convert.h"



/**
    convertation table for binary to ASCII-hex convertation
*/
const unsigned char uint4_to_hex[] =
{
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

/**
	converts a usigned 8 bit value to hexadecimal printable characters
	@return number of characters created(is constant 2)
*/
unsigned char uint8_to_hex(char *dst, unsigned char src)
{
	dst[1] = uint4_to_hex[src & 0x0F];
	src >>= 4;
	dst[0] = uint4_to_hex[src & 0x0F];

	return 2;
}

/**
	converts a usigned 16 bit value to hexadecimal printable characters

	@return number of characters created(is constant 4)
*/
unsigned char uint16_to_hex(char *dst, unsigned short src)
{
	dst[3] = uint4_to_hex[src & 0x000F];
	src >>= 4;
	dst[2] = uint4_to_hex[src & 0x000F];
	src >>= 4;
	dst[1] = uint4_to_hex[src & 0x000F];
	src >>= 4;
	dst[0] = uint4_to_hex[src & 0x000F];

	return 4;
}

/**
	converts a usigned 8 bit value to decimal printable characters

	@return number of characters created(is constant 3)
*/
unsigned char uint8_to_dec(char *dst, unsigned char src)
{
	dst[2] = uint4_to_hex[src % 10];
	src /= 10;
	dst[1] = uint4_to_hex[src % 10];
	src /= 10;
	dst[0] = uint4_to_hex[src % 10];

	return 3;
}

/**
	converts a unsigned 8 bit value to decimal printable characters
	but limits to 2 last significant characters
	@return number of characters created(is constant 2)
*/
unsigned char uint8_to_2dec(char *dst, unsigned char src)
{
	dst[1] = uint4_to_hex[src % 10];
	src /= 10;
	dst[0] = uint4_to_hex[src % 10];

	return 2;
}

/**
	converts decimal printable characters to unsigned 8 bit value
	but limits to 2 last significant characters
	@return number of characters converted 0 if illegal oterwise 2
*/
unsigned char dec2_to_uint8(char *dst, char *src)
{
	*dst = 0;

	if(src[0] != 0)
	{
		if((src[1] >= '0') && (src[1] <= '9'))
		{
			*dst = (src[1] - '0');
			if((src[0] >= '0') && (src[0] <= '9'))
			{
				*dst += (src[0] - '0') * 10;
				return 2;
			}
		}
	}
	return 0;
}

/**
	converts decimal printable characters to unsigned 16 bit value
	but limits to 4 last significant characters
	@return number of characters converted 0 if illegal oterwise 4
*/
unsigned char dec4_to_uint16(unsigned short *dst, char *src)
{
    bool is_blank = false;
    unsigned char digits = 0;
	*dst = 0;

    while(*src)
    {
        if((*src == ' ') && (digits == 0))
        {
            is_blank = true;
        }
        if((*src >= '0') && (*src <= '9') && (digits < 5))
        {
            *dst = (*dst * 10) + (*src - '0');
        }
        else if((*src == ' ') && (is_blank == true) && (digits < 5))
        {
            // no conversion
        }
        else
        {
            // abort illegal code
            break;
        }

        src++;
        digits++;
        if(digits == 4)
        {
            break;
        }
    }

    return digits;
}

/**
	converts a unsigned 8 bit value to decimal printable characters
	but limits to 3 last significant characters
	@return number of characters created(is constant 0..3)
*/
unsigned char dec_to_uint8(char *dst, char *src)
{
    bool is_blank = false;
	*dst = 0;

	if(src[0] != 0)
	{
		// 1st digit
        if((src[0] == ' ') || ((src[0] >= '0') && (src[0] <= '9')))
		{
			if(src[0] != ' ')
            {
                *dst = (src[0] - '0');
            }
            // 2nd digit
            if(((src[0] == ' ') && (src[1] == ' ')) || ((src[1] >= '0') && (src[1] <= '9')))
			{
    			*dst = (*dst * 10) + (src[1] - '0');

                // 3rd digit
                if((src[2] >= '0') && (src[2] <= '9'))
    			{
        			*dst = (*dst * 10) + (src[2] - '0');
                    return 3;
    			}
                return 2;
			}
            return 1;
		}
	}
	return 0;
}


/**
	converts a decimal ASCII characters to 16bit binary value representation
    the function terminates on nondigit character or not leading whitechar or maximum 5th digit
	@return number of characters converted 0..5
*/
unsigned char dec_to_uint16(unsigned short *dst, char *src)
{
    bool is_blank = false;
    unsigned char digits = 0;
	*dst = 0;

    while(*src)
    {
        if((*src == ' ') && (digits == 0))
        {
            is_blank = true;
        }
        if((*src >= '0') && (*src <= '9') && (digits < 5))
        {
            *dst = (*dst * 10) + (*src - '0');
        }
        else if((*src == ' ') && (is_blank == true) && (digits < 5))
        {
            // no conversion
        }
        else
        {
            // abort illegal code
            break;
        }

        src++;
        digits++;
    }

    return digits;
}

/**
	converts a usigned 32 bit value to decimal printable characters

	@return number of characters created(is constant 8)
*/
unsigned char uint32_to_8dec(char *dst, unsigned long src)
{
	dst[7] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[6] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[5] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[4] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[3] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[2] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[1] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[0] = uint4_to_hex[src % 10L];

	return 8;
}

/**
	converts a usigned 32 bit value to decimal printable characters

	@return number of characters created(is constant 10)
*/
unsigned char uint32_to_10dec(char *dst, unsigned long src)
{
	dst[9] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[8] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[7] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[6] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[5] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[4] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[3] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[2] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[1] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[0] = uint4_to_hex[src % 10L];

	return 10;
}

/**
	converts a usigned 16 bit value to decimal printable characters

	@return number of characters created(is constant 5)
*/
unsigned char uint16_to_dec(char *dst, unsigned short src)
{
	dst[4] = uint4_to_hex[src % 10];
	src /= 10;
	dst[3] = uint4_to_hex[src % 10];
	src /= 10;
	dst[2] = uint4_to_hex[src % 10];
	src /= 10;
	dst[1] = uint4_to_hex[src % 10];
	src /= 10;
	dst[0] = uint4_to_hex[src % 10];

	return 5;
}

/**
	converts a usigned 16 bit value to decimal printable characters

	@return number of characters created(is constant 10)
*/
unsigned char uint32_to_time(char *dst, unsigned long src)
{

	unsigned short fraction;

	/// @todo check that typecasting here makes this sense, otherwise fraction should be unsigned long
	fraction = (unsigned short)(src % 10000L);
	src /= 10000L;
	while(src > 86400L)
	{
		src -= 86400L;
	}

	uint16_to_dec(dst + 5, fraction);

	fraction = (unsigned short)(src % 60L);
	dst[5] = uint4_to_hex[fraction % 10];
	dst[4] = uint4_to_hex[fraction / 10];

	src /= 60L;
	fraction = (unsigned short)(src % 60L);
	dst[3] = uint4_to_hex[fraction % 10];
	dst[2] = uint4_to_hex[fraction / 10];

	src /= 60L;
	fraction = (unsigned short)src;
	dst[1] = uint4_to_hex[fraction % 10];
	dst[0] = uint4_to_hex[fraction / 10];

	return 10;
}

/**
	converts a usigned 32 bit value to hexadecimal printable characters

	@return number of characters created(is constant 8)
*/
unsigned char uint32_to_hex(char *dst, unsigned long src)
{
    // we assume that the compiler assigns correct low and high value
    typedef union
    {
        unsigned long _long;
        unsigned short _short[2];
    } uint32_union_t;

    uint32_union_t value;

    value._long = src;

    uint16_to_hex(&dst[0], value._short[1]);
    uint16_to_hex(&dst[4], value._short[0]);

	return 8;
}

/**
	converts a hexadecimal ascii coded 2byte string into usigned 8 bit value

	@return number of characters identified
*/
unsigned char hex_to_uint8(char *dst, char *src)
{
    if(isxdigit(src[0]) && isxdigit(src[1]))
    {
        unsigned char value = 0;

        if(src[0] <= '9')
        {
            value += ((src[0] - '0') * 0x10);
        }
        else
        {
            value += (((src[0] | 0x20) - 'a' + 0xa) * 0x10);
        }
        if(src[1] <= '9')
        {
            value += ((src[1] - '0'));
        }
        else
        {
            value += (((src[1] | 0x20) - 'a' + 0xa));
        }

        *dst = value;

        return 2;
    }

    return 0;
}

/**
	converts a hexadecimal ascii coded 4byte string into usigned 16 bit value

	@return number of characters identified
*/
unsigned char hex_to_uint16(unsigned short *dst, char *src)
{
    if(isxdigit(src[0]) && isxdigit(src[1]) && isxdigit(src[2]) && isxdigit(src[3]))
    {
        unsigned short value = 0;

        if(src[0] <= '9')
        {
            value += ((src[0] - '0') * 0x1000);
        }
        else
        {
            value += (((src[0] | 0x20) - 'a' + 0xa) * 0x1000);
        }
        if(src[1] <= '9')
        {
            value += ((src[1] - '0') * 0x100);
        }
        else
        {
            value += (((src[1] | 0x20) - 'a' + 0xa) * 0x100);
        }
        if(src[2] <= '9')
        {
            value += ((src[2] - '0') * 0x10);
        }
        else
        {
            value += (((src[2] | 0x20) - 'a' + 0xa) * 0x10);
        }
        if(src[3] <= '9')
        {
            value += (src[3] - '0');
        }
        else
        {
            value += (src[3] | 0x20) - 'a' + 0xa;
        }

        *dst = value;

        return 4;
    }

    return 0;
}

/**
	converts a hexadecimal ascii coded 8byte string into usigned 32 bit value

	@return number of characters identified
*/
unsigned char hex_to_uint32(unsigned long *dst, char *src)
{
    // we assume that the compiler assigns correct low and high value
    typedef union
    {
        unsigned long _long;
        unsigned short _short[2];
    } uint32_union_t;

    uint32_union_t value;

    if(hex_to_uint16(&value._short[1], &src[0]) && hex_to_uint16(&value._short[0], &src[4]))
    {
        *dst = value._long;

        return 8;
    }

	return 0;
}

unsigned char int32_to_dec(char *dst, signed long src)
{
	if (src < 0) {
		dst[0] = '-';
		src *= -1;
	} else {
		dst[0] = ' ';
	}
	dst[9] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[8] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[7] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[6] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[5] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[4] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[3] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[2] = uint4_to_hex[src % 10L];
	src /= 10L;
	dst[1] = uint4_to_hex[src % 10L];

	return 10;
}
