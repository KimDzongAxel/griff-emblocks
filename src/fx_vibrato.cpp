/************************************************
 * @brief: 	File with vibrato effect class
 * @date:	04-07-2015
 * @author:	K.Krefta
 ***********************************************/

#include "../inc/fx_vibrato.h"

FxVibrato::FxVibrato(float* in_ptr, float* out_ptr) {
	input_data_ptr 		= in_ptr;
	output_data_ptr 	= out_ptr;

	_rad = 0;

	_speed_knob 			= 128;
	_speed					= SINE_BASE_PERIOD + (_speed_knob * SINE_INCREMENT_PERIOD);
	_delay_line_base_ptr 	= (__fp16*)(calloc(STORAGE_SPACE_SAMPLES + 1, 2));
}

FxVibrato::~FxVibrato(void) {
	free(_delay_line_base_ptr);
}

void FxVibrato::SetBlend(uint8_t blend) {
    _blend_knob = blend;
    _blend = (float)blend / 100.0;
}

uint8_t FxVibrato::GetBlend(void) {
    return _blend_knob;
}
void FxVibrato::SetSpeed(uint8_t knob) {
	_speed_knob = knob;
	_speed = SINE_BASE_PERIOD + (_speed_knob * SINE_INCREMENT_PERIOD);
}

uint8_t FxVibrato::GetSpeed(void) {
	return _speed_knob;
}

void FxVibrato::Fx_Process(void) {
	_rad += (INTERSAMPLING_PERIOD / _speed) * 6.14;

	// As sampling period is so small compared to sine period,
	// no looping substractions 2pi -> 0 are required.
	if (_rad >= 6.14) {
		_rad -= 6.14;
	}

	/* OK, this might require some explanation.
	 *
	 * First of all, You've got to keep in mind that this function is
	 * a DSP function, called for every sample, so it can be seen
	 * as a loop with iterations (more of less~).
	 *
	 * As this function requres some storage space that requires to be shifted
	 * we are using DMA steam to to do this (simply to save some cycles).
	 *
	 * So, we shift at the end of the function, just to be prepaired for new
	 * data to come. So when new data comes, last iteration data are already
	 * shifted and we can just put data to first position.
	 *
	 * Buffer is growing in inverted direction. What it means is that the first
	 * element of table is in reality last memory address, and last element
	 * is first table pointer. Such mechanism is required as DMA address cannot
	 * be decremented ;_; only incremented.
	 */
	_delay_line_base_ptr[STORAGE_FIRST] = ((__fp16)(*input_data_ptr));

    #define __precomp_coef  81.43 //((STORAGE_SPACE_SAMPLES/2)) / 6.14

    float sin_val = sin(_rad);          // range -1:1
    sin_val = ((sin_val + 1) * (STORAGE_SPACE_SAMPLES / 4));       // range 0:2 -> range 0:2*250 = 0:500
    uint16_t index = round(sin_val) + (STORAGE_SPACE_SAMPLES/2);   // we have to add 500 bias to add base 5ms of delay

    *output_data_ptr = (((_delay_line_base_ptr[STORAGE_FIRST - index]) * _blend) + ((*input_data_ptr) * (1 - _blend)));
    #undef  __precomp_coef
	hardware->DMA_MemoryTransferRequest(&(_delay_line_base_ptr[0]), &(_delay_line_base_ptr[1]), STORAGE_SPACE_SAMPLES);
}
