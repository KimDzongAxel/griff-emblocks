#include "../Inc/i2s.h"

volatile	uint32_t tmp_adc_data[2];
volatile	int32_t adc_data[2];
volatile 	int32_t tmp_out_data[2];
volatile 	uint32_t out_data[2];

void I2S_ADC_Init(void) {
	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIOBEN | RCC_AHB1ENR_DMA1EN;
	GPIOB->MODER	|= (2 << (15 * 2)) | (2 << (13 * 2)) | (2 << (12* 2));
	GPIOB->AFR[1]	|= (5 << ((15*4)-32)) | (5 << ((13*4)-32)) | (5 << ((12*4)-32));
	GPIOC->MODER	|= (2 << (6 * 2));
	GPIOC->AFR[0]	|= (5 << (6 * 4));
	GPIOB->OSPEEDR	|= (3 << (15 * 2)) | (3 << (13 * 2)) | (3 << (12* 2));
	GPIOC->OSPEEDR	|= (3 << (6 * 2));
	GPIOB->PUPDR	|= (2 << (15 * 2)) | (2 << (13 * 2)) | (2 << (12* 2));

	NVIC->ISER[0] |= (1 << DMA1_Stream3_IRQn);// | (1 << DMA1_Stream4_IRQn);
	DMA1_Stream3->M0AR 	=	reinterpret_cast<uint32_t>(&tmp_adc_data);
	DMA1_Stream3->PAR	=	reinterpret_cast<uint32_t>(&(SPI2->DR));
	DMA1_Stream3->NDTR	=	4;
	DMA1_Stream3->CR	|= DMA_SxCR_PL | DMA_SxCR_MINC | DMA_SxCR_MSIZE_1 | DMA_SxCR_PSIZE_0 | DMA_SxCR_CIRC | DMA_SxCR_TCIE | DMA_SxCR_EN;

	RCC->APB1ENR	|= RCC_APB1ENR_SPI2EN;
	SPI2->CR2		|= SPI_CR2_TXEIE | SPI_CR2_RXDMAEN | SPI_CR2_TXDMAEN | SPI_CR2_RXNEIE | SPI_CR2_TXEIE;
	SPI2->I2SCFGR	|= SPI_I2SCFGR_I2SMOD | SPI_I2SCFGR_I2SE | SPI_I2SCFGR_I2SCFG_0 | SPI_I2SCFGR_I2SCFG_1 | SPI_I2SCFGR_DATLEN_0;
	SPI2->I2SPR		|= SPI_I2SPR_MCKOE | SPI_I2SPR_ODD | 3; // I2S_DIV = 11
}

void I2S_DAC_Init(void) {
	uint16_t _not_used = 0;
	RCC->AHB1ENR	|= RCC_AHB1ENR_GPIOCEN | RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_DMA1EN;
	GPIOC->MODER	|= (2 << (7 * 2)) | (2 << (10 * 2)) | (2 << (12* 2));
	GPIOC->AFR[0]	|= (6 << (7 * 4));
	GPIOC->AFR[1]	|= (6 << ((10*4)-32)) | (6 << ((12*4)-32));
	GPIOC->OSPEEDR	|= (3 << (7 * 2)) | (3 << (10 * 2)) | (3 << (12* 2));
	GPIOC->PUPDR	|= (2 << (7 * 2)) | (2 << (10 * 2)) | (2 << (12* 2));
	GPIOA->MODER	|= (2 << (15u * 2));
	GPIOA->AFR[1]	|= (6 << ((15*4)-32));
	GPIOA->OSPEEDR	|= (3 << (15u * 2));


	NVIC->ISER[1] |= (1 << (DMA1_Stream7_IRQn-32));// | (1 << DMA1_Stream4_IRQn);
	NVIC->ISER[1] |= (1 << (SPI3_IRQn-32));
	DMA1_Stream7->M0AR 	=	reinterpret_cast<uint32_t>(out_data);
	DMA1_Stream7->PAR	=	reinterpret_cast<uint32_t>(&(SPI3->DR));
	DMA1_Stream7->NDTR	=	2;
	DMA1_Stream7->CR	= DMA_SxCR_DIR_0 | DMA_SxCR_PL | DMA_SxCR_MINC | DMA_SxCR_MSIZE_0 | DMA_SxCR_PSIZE_0 | DMA_SxCR_CIRC | DMA_SxCR_TCIE;
	DMA1_Stream7->CR	|= DMA_SxCR_EN;

	RCC->APB1ENR	|= RCC_APB1ENR_SPI3EN;
	SPI3->SR 		 = 0;
	SPI3->CR2		|= SPI_CR2_TXDMAEN;// | SPI_CR2_TXEIE;
	SPI3->I2SPR		|= SPI_I2SPR_MCKOE | SPI_I2SPR_ODD | 3; // I2S_DIV = 11
	SPI3->I2SCFGR	|= SPI_I2SCFGR_I2SMOD | SPI_I2SCFGR_I2SE | SPI_I2SCFGR_I2SCFG_1 | SPI_I2SCFGR_DATLEN_0 | SPI_I2SCFGR_CHLEN;
	_not_used = SPI3->DR;
}

void SPI3_IRQHandler(void) {
	//if (SPI3->SR & SPI_SR_TXE)
		//SPI3->DR = 0x5500;
}

void DAC_SetOutput(int32_t data[2]) {
	tmp_out_data[0] = data[0];
	tmp_out_data[1] = data[1];
}

void DMA1_Stream3_IRQHandler(void) {
	uint16_t* odata_ptr = (uint16_t*)tmp_adc_data;
	uint16_t* ndata_ptr = (uint16_t*)adc_data;
	//uint32_t* xdata_ptr;
	uint32_t x = adc_data[0];
	uint32_t y = adc_data[1];
	if (DMA1->LISR & DMA_LIFCR_CTCIF3) {
		/*(*(ndata_ptr)) = __REV16(*(odata_ptr));
		(*(ndata_ptr+1)) = __REV16(*(odata_ptr+1));
		(*(ndata_ptr+2)) = __REV16(*(odata_ptr+2));
		(*(ndata_ptr+3)) = __REV16(*(odata_ptr+3));*/
		*(ndata_ptr + 0) = *(odata_ptr + 1);
		*(ndata_ptr + 1) = *(odata_ptr + 0);
		*(ndata_ptr + 2) = *(odata_ptr + 3);
		*(ndata_ptr + 3) = *(odata_ptr + 2);
		//(*(xdata_ptr)) 		/= 2;
		//(*(xdata_ptr + 1))	/=2;
		//adc_data[0]  /= 2;
		//adc_data[1]  /= 2;
		DMA1->LIFCR |= DMA_LIFCR_CTCIF3;
	}
}

void DMA1_Stream7_IRQHandler(void) {
	if (DMA1->HISR & DMA_HIFCR_CTCIF7) {
		uint32_t tmp[2];
		uint16_t* ndata_ptr = (uint16_t*)tmp;
		uint16_t* odata_ptr = (uint16_t*)tmp_out_data;
		*(ndata_ptr + 0) = (*(odata_ptr + 1));
		*(ndata_ptr + 1) = (*(odata_ptr + 0));
		*(ndata_ptr + 2) = (*(odata_ptr + 3));
		*(ndata_ptr + 3) = (*(odata_ptr + 2));
		out_data[0] = (uint32_t)tmp[0];
		out_data[1] = (uint32_t)tmp[1];
		DMA1->HIFCR |= DMA_HIFCR_CTCIF7;
	}

}
