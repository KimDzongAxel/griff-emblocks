#include "../Inc/Button.h"

Button::Button(GPIO_TypeDef* GPIO_LED_G, uint8_t pin_led_g, GPIO_TypeDef* GPIO_LED_R, uint8_t pin_led_r, GPIO_TypeDef* GPIOBTN, uint8_t pin_btn) {
	switch((uint32_t)GPIO_LED_G) {
		case GPIOA_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
			break;
		case GPIOB_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
			break;
		case GPIOC_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
			break;
		case GPIOD_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
			break;
		case GPIOE_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
			break;
		case GPIOF_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
			break;
		case GPIOG_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
			break;
		case GPIOH_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
			break;
		case GPIOI_BASE:
			RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;
			break;
		}
	switch((uint32_t)GPIO_LED_R) {
	case GPIOA_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
		break;
	case GPIOB_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
		break;
	case GPIOC_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
		break;
	case GPIOD_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
		break;
	case GPIOE_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
		break;
	case GPIOF_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
		break;
	case GPIOG_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
		break;
	case GPIOH_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
		break;
	case GPIOI_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;
		break;
	}
	switch((uint32_t)GPIOBTN) {
	case GPIOA_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
		break;
	case GPIOB_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
		break;
	case GPIOC_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;
		break;
	case GPIOD_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIODEN;
		break;
	case GPIOE_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOEEN;
		break;
	case GPIOF_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;
		break;
	case GPIOG_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;
		break;
	case GPIOH_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;
		break;
	case GPIOI_BASE:
		RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;
		break;
	}
	GPIO_LED_G->MODER &=~(3 << (2 * pin_led_g));
	GPIO_LED_G->MODER |= (1 << (2 * pin_led_g));
	GPIO_LED_R->MODER &=~(3 << (2 * pin_led_r));
	GPIO_LED_R->MODER |= (1 << (2 * pin_led_r));
	GPIOBTN->MODER &=~(3 << (2 * pin_btn));
	GPIOBTN->MODER &=~(1 << (2 * pin_btn));
	GPIOBTN->OTYPER |= (1 << pin_btn);
	GPIOBTN->PUPDR  &=~(3 << (2 * pin_btn));
	GPIOBTN->PUPDR  |= (1 << (2 * pin_btn));
	GPIOBTN->BSRRL	|= (1 << (pin_btn));

	_GPIO_BTN = GPIOBTN;
	_GPIO_LED_G = GPIO_LED_G;
	_GPIO_LED_R = GPIO_LED_R;

	_r_led_pin = pin_led_r;
	_g_led_pin = pin_led_g;
	_btn_pin = pin_btn;
	_color = COLOR_NONE;
	_bounce_cnt = 0;
}

bool Button::GetState(void) {
	if (_state) {
		_bounce_cnt = 0;
		_state = false;
		return true;
	} else {
		return false;
	}
}

void Button::SetColor(Color c) {
	_color = c;
}

void Button::TimerTick() {
	if (_GPIO_BTN->IDR & (1 << _btn_pin)) {
		if (_bounce_cnt < BOUNCE_THRESH)
		_bounce_cnt++;
	} else {
		if (_bounce_cnt > ((-1) * BOUNCE_THRESH))
		_bounce_cnt--;
	}
	if (_bounce_cnt == BOUNCE_THRESH) {
		_state = true;
	} else {
		_state = false;
	}
	switch(_color) {
	case COLOR_NONE:
		_GPIO_LED_G->BSRRH |= (1 << _g_led_pin);
		_GPIO_LED_R->BSRRH |= (1 << _r_led_pin);
		break;
	case COLOR_GREEN:
		_GPIO_LED_G->BSRRL |= (1 << _g_led_pin);
		_GPIO_LED_R->BSRRH |= (1 << _r_led_pin);
		break;
	case COLOR_RED:
		_GPIO_LED_G->BSRRH |= (1 << _g_led_pin);
		_GPIO_LED_R->BSRRL |= (1 << _r_led_pin);
		break;
	case COLOR_ORANGE: {
		uint8_t tmp = 0;
		_GPIO_LED_G->ODR ^= (1 << _g_led_pin);
		if (_GPIO_LED_G->ODR & (1 << _g_led_pin))
			_GPIO_LED_R->BSRRH |= (1 << _r_led_pin);
		else
			_GPIO_LED_R->BSRRL |= (1 << _r_led_pin);
		break;
		}
	}
}
