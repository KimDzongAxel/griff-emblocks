#include "../Inc/elist.h"

EList::EList(void) {
	head = NULL;
	count = 0;
}

void EList::PushValue(void* data_ptr){
	count++;
	Node *n = reinterpret_cast<Node*>(calloc(sizeof(Node), 1));   // create new Node
	if (n == NULL)
		return;
	n->data_ptr = data_ptr;             // set value
	n->next = head;         // make the node point to the next node.
							//  If the list is empty, this is NULL, so the end of the list --> OK
	head = n;               // last but not least, make the head point at the new node.
}

void* EList::PopBotton(){
	Node *n = head;
	if (n == NULL) {
		count = 0;
		return NULL;
	} else {
		while(n->next != NULL) {
			n = n->next;
		}
		void* ret_ptr = n->data_ptr;
		if (n == head) {
			count = 0;
			free(n);
			head = NULL;
		} else {
			Node *prev = head;
			while(!(prev->next == n)) {
				prev = prev->next;
			}
			free(n);
			prev->next = NULL;
		}
		return ret_ptr;
	}
}

void* EList::PopValue(){
	if (head == NULL) {
		return NULL;
	} else {
	count--;
	Node *n = head;
	void* ret_ptr = n->data_ptr;
	head = head->next;
	free(n);
	return ret_ptr;
	}
}

