#include "../Inc/fx_ovrdrv.h"
#include <cmath>
FxOverdrive::FxOverdrive(float* in_ptr, float* out_ptr) {
	input_data_ptr 		= in_ptr;
	output_data_ptr 	= output_data_ptr;

	_gain_knob			= 0;

	_cut_lvl_knob		= 50;

	_tone_knob			= 50;
	set_tone(_tone_knob);

	_xh					= 0;
}

void FxOverdrive::Fx_Process(void) {
	//  fetching data
	_sample = *input_data_ptr;

	//  simulating gain (quasi infinite)
	_sample *= _gain;

	//  simulating saturation of non-linear element
	if (_sample > 0) {
		if (_sample > _cut_lvl) {
			_sample = _cut_lvl;
		}
	} else {
		if (_sample < -_cut_lvl) {
			_sample = -_cut_lvl;
		}
	}

	// low pass filter (tone knob)
	float xh_new = _sample - _tone_c * _xh;
	float ap_y = _tone_c * xh_new + _xh;
	_xh = xh_new;
	_sample = 0.5 * (_sample + ap_y);

	// blend of signal + ovrdrv
	_sample = (*input_data_ptr * _blend) + (_sample * (1-_blend));

	*output_data_ptr = _sample;
}

void FxOverdrive::set_blend(uint8_t blend) {
	if (blend > 100) {
		_blend_knob = 100;
	} else {
		_blend_knob = blend;
	}
	_blend = _blend_knob * 0.01;
}

void FxOverdrive::set_cut(uint8_t knob) {
	if (knob > 100) {
		_cut_lvl_knob = 100;
	} else {
		_cut_lvl_knob = knob;
	}

	_cut_lvl = (knob * 0.01) * 16777216;
}
void FxOverdrive::set_tone(uint8_t knob) {
	if (knob > 100) {
		knob = 100;
	}
	_tone_knob = knob;
	float Wc =  knob2freq[knob] / Fs;
	_tone_c = (tan(M_PI * Wc * 0.5) - 1) / (tan(M_PI * Wc * 0.5) + 1);
}
